//
//  SURepositotyOwner.h
//  GitHubTest
//
//  Created by sergey on 14.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EKMappingProtocol.h"
/*!
 * @brief Parsed object for repository owner
 */
@interface SURepositotyOwner : NSObject <EKMappingProtocol>
/*!
 * @brief owner's login
 */
@property (nonatomic,copy) NSString* login;
/*!
 * @brief owner's avatar url
 */
@property (nonatomic,copy) NSString* avatar_url;
@end

//
//  SURepository.h
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EKMappingProtocol.h"
#import "SURepositotyOwner.h"

/*!
 * @brief Parsed object for repository
 */
@interface SURepository : NSObject <EKMappingProtocol>
/*!
 * @brief name of repository
 */
@property (nonatomic,copy) NSString* name;
/*!
 * @brief stars count
 */
@property (nonatomic,readwrite) NSUInteger stargazers_count;
/*!
 * @brief repository owner
 */
@property (nonatomic,strong) SURepositotyOwner* owner;
/*!
 * @brief repository description
 */
@property (nonatomic,copy) NSString* repoDescription;
/*!
 * @brief repository programming language
 */
@property (nonatomic,copy) NSString* language;
/*!
 * @brief repository creation date
 */
@property (nonatomic,strong) NSDate* created_at;
/*!
 * @brief repository update date
 */
@property (nonatomic,strong) NSDate* updated_at;
/*!
 * @brief repository watchers count
 */
@property (nonatomic,assign) NSUInteger watchers_count;
/*!
 * @brief repository web link
 */
@property (nonatomic,copy) NSString* html_url;
@end

//
//  SUSearchResult.h
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EKMappingProtocol.h"
#import "SURepository.h"

/*!
 * @brief Parsed object for server response
 */
@interface SUSearchResult : NSObject <EKMappingProtocol>
/*!
 * @brief total repositories count
 */
@property (nonatomic,readwrite) NSUInteger total_count;
/*!
 * @brief SURepository objects
 */
@property (nonatomic,strong) NSArray* repositories;

@end

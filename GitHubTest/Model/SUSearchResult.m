//
//  SUSearchResult.m
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUSearchResult.h"

@implementation SUSearchResult

+(EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[
                                          @"total_count"
                                          ]];
        [mapping hasMany:[SURepository class] forKeyPath:@"items" forProperty:@"repositories"];
    }];
}


@end

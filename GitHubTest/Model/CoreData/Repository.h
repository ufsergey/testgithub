//
//  Repository.h
//  GitHubTest
//
//  Created by sergey on 21.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Owner, Request;
/*!
 * @brief Managed object for repository
 */
@interface Repository : NSManagedObject
/*!
 * @brief repository name
 */
@property (nonatomic, retain) NSString * name;
/*!
 * @brief stargazers count
 */
@property (nonatomic, retain) NSNumber * stargazers_count;
/*!
 * @brief repository description
 */
@property (nonatomic, retain) NSString * repoDescription;
/*!
 * @brief repository programming language
 */
@property (nonatomic, retain) NSString * language;
/*!
 * @brief repository creation date
 */
@property (nonatomic, retain) NSDate * created_at;
/*!
 * @brief repository update date
 */
@property (nonatomic, retain) NSDate * updated_at;
/*!
 * @brief repository watchers count
 */
@property (nonatomic, retain) NSNumber * watchers_count;
/*!
 * @brief repository webSite url
 */
@property (nonatomic, retain) NSString * html_url;
/*!
 * @brief request which found the repository
 */
@property (nonatomic, retain) Request *request;
/*!
 * @brief repository owner
 */
@property (nonatomic, retain) Owner *owner;

@end

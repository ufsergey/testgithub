//
//  Owner.m
//  GitHubTest
//
//  Created by sergey on 21.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "Owner.h"
#import "Repository.h"


@implementation Owner

@dynamic login;
@dynamic avatar_url;
@dynamic repository;

@end

//
//  Request.h
//  GitHubTest
//
//  Created by sergey on 22.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Repository;
/*!
 * @brief Managed object for request
 */
@interface Request : NSManagedObject

/*!
 * @brief date of search
 */
@property (nonatomic, retain) NSDate * requestDate;
/*!
 * @brief total founded repositories count
 */
@property (nonatomic, retain) NSString * requestString;
/*!
 * @brief set of founded repositories
 */
@property (nonatomic, retain) NSNumber * totalCount;
/*!
 * @brief set of founded repositories
 */
@property (nonatomic, retain) NSSet *repositories;
@end

@interface Request (CoreDataGeneratedAccessors)

- (void)addRepositoriesObject:(Repository *)value;
- (void)removeRepositoriesObject:(Repository *)value;
- (void)addRepositories:(NSSet *)values;
- (void)removeRepositories:(NSSet *)values;

@end

//
//  Request.m
//  GitHubTest
//
//  Created by sergey on 22.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "Request.h"
#import "Repository.h"


@implementation Request

@dynamic requestDate;
@dynamic requestString;
@dynamic totalCount;
@dynamic repositories;

@end

//
//  Repository.m
//  GitHubTest
//
//  Created by sergey on 21.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "Repository.h"
#import "Owner.h"
#import "Request.h"


@implementation Repository

@dynamic name;
@dynamic stargazers_count;
@dynamic repoDescription;
@dynamic language;
@dynamic created_at;
@dynamic updated_at;
@dynamic watchers_count;
@dynamic html_url;
@dynamic request;
@dynamic owner;

@end

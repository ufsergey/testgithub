//
//  Owner.h
//  GitHubTest
//
//  Created by sergey on 21.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Repository;
/*!
 * @brief Managed object for repository owner
 */
@interface Owner : NSManagedObject
/*!
 * @brief owner login
 */
@property (nonatomic, retain) NSString * login;
/*!
 * @brief owner avatar url
 */
@property (nonatomic, retain) NSString * avatar_url;
/*!
 * @brief repository
 */
@property (nonatomic, retain) Repository *repository;

@end

//
//  SURepositotyOwner.m
//  GitHubTest
//
//  Created by sergey on 14.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SURepositotyOwner.h"

@implementation SURepositotyOwner
+(EKObjectMapping *)objectMapping {
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[
                                          @"login",@"avatar_url"
                                          ]];
        
    }];
}
@end

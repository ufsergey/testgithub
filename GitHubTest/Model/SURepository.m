//
//  SURepository.m
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SURepository.h"

@implementation SURepository

+(EKObjectMapping *)objectMapping {
    
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[
                                          @"stargazers_count",
                                          @"name",
                                          @"language",
                                          @"watchers_count",
                                          @"html_url"
                                      ]];
        [mapping mapKeyPath:@"description" toProperty:@"repoDescription"];
        [mapping mapKeyPath:@"created_at" toProperty:@"created_at" withValueBlock:^id(NSString *key, id value) {
            
            NSDate *date = [SURepository dateFromString:value];
            return date;
        }];
        [mapping mapKeyPath:@"updated_at" toProperty:@"updated_at" withValueBlock:^id(NSString *key, id value) {
            
            NSDate *date = [SURepository dateFromString:value];
            return date;
        }];
        
        [mapping hasOne:[SURepositotyOwner class] forKeyPath:@"owner"];
   
    }];
}

+(NSDate*) dateFromString:(NSString*) str{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *date = [formatter dateFromString:str];
    return date;
}

@end

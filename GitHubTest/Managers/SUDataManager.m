//
//  SUDataManager.m
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUDataManager.h"
#import "SUAPIManager.h"
#import "SUSearchResult.h"
#import "EKMapper.h"
#import <UIKit/UIKit.h>
#import "Repository.h"
#import "Request.h"
#import "Owner.h"

NSString* const SUSearchResultsRecivedNotification = @"SUSearchResultsRecivedNotification";
NSString* const SULastQueryKey = @"SULastQueryKey";

@interface SUDataManager ()
@property (nonatomic,assign) NSUInteger currentAttemptsCount;
@end

@implementation SUDataManager

static SUDataManager * manager = nil;

+(SUDataManager*)sharedManager {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        manager = [[self alloc] init];
        
    });
    return manager;
}

- (id)init {
    if (self = [super init]) {
        self.results = [NSMutableArray array];
    }
    return self;
}
-(void)clearSearchResults {
    [self.results removeAllObjects];
}
/*!
 * @brief Sort founded repositories and add to results. Post notification SUSearchResultsRecivedNotification
 * @param request current request
 */
-(void) updateSearchResultsForRequest:(Request*) request{

    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"stargazers_count"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [[request.repositories allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    [self.results addObjectsFromArray:sortedArray];
    
   
    [[NSNotificationCenter defaultCenter] postNotificationName:SUSearchResultsRecivedNotification object:nil];
    
}

-(void) searchRepositoriesWithQuery:(NSString*) query page:(NSUInteger) page{
    
    NSString* requestString = [query stringByAppendingString:[NSString stringWithFormat:@"page%lu",(unsigned long)page]];
    
    Request* request = [self getRequestWithRequestString:requestString];
    
    if (request) {
       
        [self updateSearchResultsForRequest:request];
        
    } else {
        
        self.currentAttemptsCount = 1;
        [self searchRepositoriesOnServerWithQuery:query page:page];
        
    }
}
/*!
 * @brief Search repositories on server
 * @param query  search word
 * @param page current page
 */
-(void) searchRepositoriesOnServerWithQuery:(NSString*) query page:(NSUInteger) page{
    
    NSString* requestString = [query stringByAppendingString:[NSString stringWithFormat:@"page%lu",(unsigned long)page]];    __weak typeof(self) weakSelf = self;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [[SUAPIManager sharedManager] getRepositoriesWithQuery:query page:page success:^(NSDictionary *responseDict) {
        
        SUSearchResult *searchResult = [EKMapper objectFromExternalRepresentation:responseDict
                                                                      withMapping:[SUSearchResult objectMapping]];
        
        NSLog(@"ok, found %lu repositories",(unsigned long)searchResult.total_count);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        Request* request = [weakSelf saveSearchResult:searchResult forRequestString:requestString];
        
        [weakSelf updateSearchResultsForRequest:request];
        
    } failure:^(NSError *error) {
        if (weakSelf.currentAttemptsCount < weakSelf.repeatCount) {
            NSLog(@"error, try again");
            weakSelf.currentAttemptsCount ++;
            [weakSelf searchRepositoriesOnServerWithQuery:query page:page];
        } else {
            NSLog(@"error %@",error.localizedDescription);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Search error", nil)
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
            [alert show];
        }
    }];
}

#pragma mark - Helpers
/*!
 * @brief Create Request,Response, Owner entities and fill data from searchResult for requestString
 * @param searchResult  SUSearchResult object from server response with repositories
 * @param requestString string with search word and cuttent page
 * @return Request entity
 */
-(Request*) saveSearchResult:(SUSearchResult*) searchResult forRequestString:(NSString*) requestString {

    Request* request = [SUDataManager createRequestWithRequestString:requestString];
    if (request) {
        [SUDataManager createRepositoriesForRequest:request withSearchResult:searchResult];
    }
    return request;
}

#pragma mark - LocalData
/*!
 * @brief Find Request entity in local store for requestString. If it found, check expiration date. If expiration date is old , delete found entity, else return first found entity.
 * @param requestString string with search word and cuttent page
 * @return Request entity or nil if request not found or entity is old
 */
-(Request*) getRequestWithRequestString:(NSString*) requestString{
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"requestString ==[c] %@",requestString];
    NSArray* requests = [Request MR_findAllWithPredicate:predicate];
    NSMutableArray* result = [NSMutableArray new];
    
    for (Request* request in requests) {
        NSDate* expirationDate = [request.requestDate dateByAddingTimeInterval:self.expirationInterval];
        if ([expirationDate compare:[NSDate date]] == NSOrderedAscending) {
            NSLog(@"request %@ is old",request.requestString);
            [request MR_deleteEntity];
        } else {
            [result addObject:request];
        }
    }
    [SUDataManager saveContext];
    
    if ([result count]>0) {
        return result[0];
    } else {
        return nil;
    }
}
/*!
 * @brief Fill Request entity params with searchResult response from server
 * @param request Request entity
 * @param searchResult  SUSearchResult object from server response with repositories
 */
+(void) createRepositoriesForRequest:(Request*) request withSearchResult:(SUSearchResult*) searchResult {

    for (SURepository* repository in searchResult.repositories) {
        Repository* localRepo = [Repository MR_createEntity];
        Owner* owner = [Owner MR_createEntity];
        localRepo.name = repository.name;
        localRepo.stargazers_count = [NSNumber numberWithInteger:repository.stargazers_count];
        localRepo.repoDescription = repository.repoDescription;
        localRepo.language = repository.language;
        localRepo.created_at = repository.created_at;
        localRepo.updated_at = repository.updated_at;
        localRepo.watchers_count = [NSNumber numberWithInteger:repository.watchers_count];
        localRepo.html_url = repository.html_url;
        owner.login = repository.owner.login;
        owner.avatar_url = repository.owner.avatar_url;
        owner.repository = localRepo;
        localRepo.owner = owner;
        localRepo.request = request;
        
    }
    request.totalCount = [NSNumber numberWithInteger:searchResult.total_count];
    [SUDataManager saveContext];
}
/*!
 * @brief Create new Request entity and add requestString to params
 * @param requestString string with search word and cuttent page
 * @return Request entity
 */
+(Request*) createRequestWithRequestString:(NSString*) requestString {

    Request* request = [Request MR_createEntity];
    request.requestString = requestString;
    request.requestDate = [NSDate date];
    [SUDataManager saveContext];
    return request;
}
/*!
 * @brief Save context
 */
+ (void)saveContext {
    NSManagedObjectContext* context = [NSManagedObjectContext MR_defaultContext];
    [context MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
        
    }];
}
+(void) deleteAll {
    [Request MR_truncateAll];
    [SUDataManager saveContext];
}
+(void) clearOldData{

    NSDate* expirationDate = [[NSDate date] dateByAddingTimeInterval: - [SUDataManager sharedManager].expirationInterval];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"requestDate < %@",expirationDate];
    NSArray* requests = [Request MR_findAllWithPredicate:predicate];
    
    for (Request* request in requests) {
        [request MR_deleteEntity];
    }
    [SUDataManager saveContext];
}


#pragma mark - LastQuery
-(void)saveLastQuery:(NSString *)query {
    [[NSUserDefaults standardUserDefaults]setObject:query forKey:SULastQueryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)getLastQueryResults {
    NSString* query = [[NSUserDefaults standardUserDefaults] objectForKey:SULastQueryKey];
    if (query) {
        NSString* requestString = [query stringByAppendingString:[NSString stringWithFormat:@"page1"]];
        
        Request* request = [self getRequestWithRequestString:requestString];
        
        if (request) {
            
            [self updateSearchResultsForRequest:request];
            return query;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}
@end

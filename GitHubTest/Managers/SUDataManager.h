//
//  SUDataManager.h
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Repository.h"
extern NSString* const SUSearchResultsRecivedNotification;
@class SURepository;
/*!
 * @brief SingleTone manager for working with data model
 */
@interface SUDataManager : NSObject
/*!
 * @brief founded repositories
 */
@property (nonatomic,strong) NSMutableArray* results;
/*!
 * @brief selected repository
 */
@property (nonatomic,strong) Repository* selectedRepository;
/*!
 * @brief expiration interval in seconds
 */
@property (nonatomic,assign) NSTimeInterval const expirationInterval;
/*!
 * @brief count of connection attempts
 */
@property (nonatomic,assign) NSUInteger const repeatCount;
/*!
 * @brief save last query word in NSUserDefaults
 * @param query  search word
 */
-(void) saveLastQuery:(NSString*) query;
/*!
 * @brief get repositories for last query in NSUserDefaults. If request is not old set results array. Post notification SUSearchResultsRecivedNotification
 * @return query - last search word
 */
-(NSString*) getLastQueryResults;
/*!
 * @brief create SUDataManager object
 */
+(SUDataManager*)sharedManager;
/*!
 * @brief Begin search repositories. Check repositories with query and page in local store. If repositories is old request from server
 * @param query  search word
 * @param page current page
 */
-(void) searchRepositoriesWithQuery:(NSString*) query page:(NSUInteger) page;
/*!
 * @brief Delete all results
 */
-(void)clearSearchResults;
/*!
 * @brief delete all from local store,  not used
 */
+(void) deleteAll;
/*!
 * @brief delete all old requests from local store
 */
+(void) clearOldData;
@end

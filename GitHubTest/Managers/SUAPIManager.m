//
//  SUAPIManager.m
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUAPIManager.h"


NSString* const SUConnectionNotReachableNotification = @"SUConnectionNotReachableNotification";
@interface SUAPIManager()

@property (nonatomic,strong) AFHTTPRequestOperationManager *manager;

@end

@implementation SUAPIManager

static SUAPIManager * manager = nil;

+(id)sharedManager {
    
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (id)init {
    if (self = [super init]) {
        
        self.manager  = [AFHTTPRequestOperationManager manager];
        [self setReachabilityStatusBlock];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return self;
}

-(void) setReachabilityStatusBlock {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        if (status == AFNetworkReachabilityStatusNotReachable) {
            [[NSNotificationCenter defaultCenter] postNotificationName:SUConnectionNotReachableNotification object:nil];
        }
    }];
}



-(void) getRepositoriesWithQuery:(NSString*) query
                            page:(NSUInteger) page
                         success:(void (^)(NSDictionary* responseDict))success
                         failure:(void (^)(NSError *error))failure
{

    self.manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary* params = @{
                             @"sort":@"stars",
                             @"order":@"desc",
                             @"page":[NSNumber numberWithUnsignedInteger:page]};
    
    NSString * urlString = [self.baseURL stringByAppendingString:[NSString stringWithFormat:@"search/repositories?q=%@+in:name,description",encodeToPercentEscapeString(query)]];
    
    [self.manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            success(responseObject);
        } else {
            failure(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure(error);
    }];

}

NSString *encodeToPercentEscapeString(NSString *string) {
    return (__bridge NSString *)
    CFURLCreateStringByAddingPercentEscapes(NULL,
                                            (CFStringRef) string,
                                            NULL,
                                            (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                            kCFStringEncodingUTF8);
}




@end

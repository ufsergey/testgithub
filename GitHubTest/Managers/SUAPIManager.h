//
//  SUAPIManager.h
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const SUConnectionNotReachableNotification;

/*!
 * @class SUAPIManager
 * @brief SUAPIManager SingleTone manager for working with server API
 */
@interface SUAPIManager : NSObject
/*!
 * @brief base URL for server
 */
@property (nonatomic,copy) NSString* baseURL;
/*!
 * @brief create SUAPIManager object
 */
+(id)sharedManager;
/*!
 * @brief Create GET request for searching repositories
 * @param query search word
 * @param page current page for request
 * @param success block for success response
 * @param failure block for error response
 */
-(void) getRepositoriesWithQuery:(NSString*) query
                            page:(NSUInteger) page
                         success:(void (^)(NSDictionary* responseDict))success
                         failure:(void (^)(NSError *error))failure;

@end

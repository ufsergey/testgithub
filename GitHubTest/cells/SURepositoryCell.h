//
//  SURepositoryCell.h
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 * @brief Custom cell for main screen
 */
@interface SURepositoryCell : UITableViewCell
/*!
 * @brief repository name
 */
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
/*! 
 * @brief repository stars count
 */
@property (weak, nonatomic) IBOutlet UILabel *labelStars;
/*! 
 * @brief repository autor avatar
 */
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAvatar;
/*!
 * @brief repository autor title
 */
@property (weak, nonatomic) IBOutlet UILabel *labelAutorCaption;
/*! 
 * @brief repository stars title
 */
@property (weak, nonatomic) IBOutlet UILabel *labelStarsCaption;
/*! 
 * @brief repository name title
 */
@property (weak, nonatomic) IBOutlet UILabel *labelTitleCaption;
/*! 
 * @brief repository autor login
 */
@property (weak, nonatomic) IBOutlet UILabel *labelAutor;

@end

//
//  SUDetailsVC.h
//  GitHubTest
//
//  Created by sergey on 14.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SURepository.h"
#import "SUBaseVC.h"
#import "SUMasterVC.h"
/*!
 * @brief details view controller , show details for selected repository
 */
@interface SUDetailsVC : SUBaseVC <UISplitViewControllerDelegate,RepositorySelectionDelegate>
@end


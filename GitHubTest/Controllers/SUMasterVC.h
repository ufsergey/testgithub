//
//  SUMasterVC.h
//  GitHubTest
//
//  Created by sergey on 29.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUBaseVC.h"

@protocol RepositorySelectionDelegate <NSObject>
@required
-(void)repositorySelected;
@end

/*!
 * @brief Left(master) view controller , show list of repositories
 */
@interface SUMasterVC : SUBaseVC<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,assign) NSUInteger currentPage;
@property (nonatomic,weak) id<RepositorySelectionDelegate>delegate;
@end

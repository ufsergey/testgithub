//
//  SUBaseVC.m
//  GitHubTest
//
//  Created by sergey on 15.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUBaseVC.h"

@interface SUBaseVC ()

@end

@implementation SUBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) connectionLost {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error"
                                                    message:@"Check your internet connection"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end

//
//  ViewController.h
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUBaseVC.h"
/*!
 * @brief Main view controller , show list of repositories
 */
@interface SUMainVC : SUBaseVC


@end


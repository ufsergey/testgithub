//
//  SUBaseVC.h
//  GitHubTest
//
//  Created by sergey on 15.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUAPIManager.h"
/*!
 * @brief Base view controller
 */
@interface SUBaseVC : UIViewController
/*!
 * @brief Showing error message. Called then SUConnectionNotReachableNotification is triggered
 */
-(void) connectionLost;
@end

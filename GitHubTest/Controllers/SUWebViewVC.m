//
//  SUWebViewVC.m
//  GitHubTest
//
//  Created by sergey on 15.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUWebViewVC.h"
#import "SUDataManager.h"
#import "SURepository.h"
#import "Repository.h"
#import "Request.h"

@interface SUWebViewVC ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation SUWebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:[SUDataManager sharedManager].selectedRepository.html_url];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:urlRequest];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionLost) name:SUConnectionNotReachableNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SUConnectionNotReachableNotification object:nil];
}
@end

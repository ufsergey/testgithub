//
//  ViewController.m
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUMainVC.h"
#import "SURepositoryCell.h"
#import "SURepository.h"

#import "SUDetailsVC.h"
#import "Repository.h"
#import "Request.h"
#import "Owner.h"

@interface SUMainVC () <UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,assign) NSUInteger currentPage;
@end

@implementation SUMainVC

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    self.searchBar.delegate = self;
    self.searchBar.text = [[SUDataManager sharedManager] getLastQueryResults];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchCompleted) name:SUSearchResultsRecivedNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionLost) name:SUConnectionNotReachableNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SUConnectionNotReachableNotification object:nil];
}

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self name:SUSearchResultsRecivedNotification object:nil];
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[SUDataManager sharedManager].results count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    SURepositoryCell* cell = [tableView dequeueReusableCellWithIdentifier:@"SURepositoryCell"];
    Repository* repository = [[SUDataManager sharedManager].results objectAtIndex:indexPath.row];
    cell.labelTitle.text = repository.name;
    cell.labelStars.text = [NSString stringWithFormat:@"%lu",(long)[repository.stargazers_count integerValue]];
    cell.labelAutor.text = repository.owner.login;
    [cell.imageViewAvatar sd_setImageWithURL:[NSURL URLWithString:repository.owner.avatar_url]];
    cell.labelTitleCaption.text = [NSLocalizedString(@"Title", nil) stringByAppendingString:@":"];
    cell.labelStarsCaption.text = [NSLocalizedString(@"Stars", nil) stringByAppendingString:@":"];
    cell.labelAutorCaption.text = [NSLocalizedString(@"Autor", nil) stringByAppendingString:@":"];
    return cell;
    
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [SUDataManager sharedManager].selectedRepository = [[SUDataManager sharedManager].results objectAtIndex:indexPath.row];
    SUDetailsVC* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SUDetailsVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

/*!
 * @brief request next page if total repositories count < recived repositories count
 */
-(void) loadMore {
    
    if ([[SUDataManager sharedManager].results count]>0) {
        Repository * repository = [SUDataManager sharedManager].results[0];
        NSUInteger totalCount = [repository.request.totalCount integerValue];
        if ([[SUDataManager sharedManager].results count]< totalCount) {
            self.currentPage++;
            [[SUDataManager sharedManager] searchRepositoriesWithQuery:self.searchBar.text page:self.currentPage];
        }
    }
}


#pragma mark - UISearchBarDelegate

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if ([searchBar.text length]>0) {
        [[SUDataManager sharedManager] clearSearchResults];
        self.currentPage = 1;
        [[SUDataManager sharedManager] saveLastQuery:searchBar.text];
        [[SUDataManager sharedManager] searchRepositoriesWithQuery:searchBar.text page:self.currentPage];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [[SUDataManager sharedManager].results count]-2) {
        [self loadMore];
        NSLog(@"----------------load more");
    }
}




#pragma mark - Notifications
/*!
 * @brief refresh table View and show message if repisitories not found
 */
-(void) searchCompleted {

    if ([[SUDataManager sharedManager].results count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Search completed", nil)
                                                    message:NSLocalizedString(@"Repositories not found", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    [self.tableView reloadData];
}


@end

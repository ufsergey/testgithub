//
//  SUDetailsVC.m
//  GitHubTest
//
//  Created by sergey on 14.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "SUDetailsVC.h"
#import "SUWebViewVC.h"
#import "Owner.h"
@interface SUDetailsVC () 
@property (weak, nonatomic) IBOutlet UILabel *labelTitleCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelStarsCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelCreatedCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelUpdatedCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelLanguageCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelWatchersCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelAutorCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelDescriptionCaption;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelStars;
@property (weak, nonatomic) IBOutlet UILabel *labelCreated;
@property (weak, nonatomic) IBOutlet UILabel *labelUpdated;
@property (weak, nonatomic) IBOutlet UILabel *labelLanguage;
@property (weak, nonatomic) IBOutlet UILabel *labelWatchers;
@property (weak, nonatomic) IBOutlet UILabel *labelAutor;
@property (weak, nonatomic) IBOutlet UILabel *labelNoRepository;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAvatar;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property (weak, nonatomic) IBOutlet UIButton *buttonURL;
@property (nonatomic,strong) UIWebView* webView;
@property (weak, nonatomic) IBOutlet UIView *viewNoRepo;

@end

@implementation SUDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labelNoRepository.text = NSLocalizedString(@"Select repository from list", nil);
    self.labelTitleCaption.text = NSLocalizedString(@"Title", nil);
    self.labelStarsCaption.text = NSLocalizedString(@"Stars", nil);
    self.labelCreatedCaption.text = NSLocalizedString(@"Created at", nil);
    self.labelUpdatedCaption.text = NSLocalizedString(@"Updated at", nil);
    self.labelLanguageCaption.text = NSLocalizedString(@"Language", nil);
    self.labelWatchersCaption.text = NSLocalizedString(@"Watchers", nil);
    self.labelAutorCaption.text = NSLocalizedString(@"Autor", nil);
    self.labelDescriptionCaption.text = NSLocalizedString(@"Description", nil);
    [self.buttonURL setTitle:NSLocalizedString(@"ButtonWebView", nil) forState:UIControlStateNormal];
    
    [self fillView];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionLost) name:SUConnectionNotReachableNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SUConnectionNotReachableNotification object:nil];
}

-(void) fillView {
    if ( ! [SUDataManager sharedManager].selectedRepository) {
        self.viewNoRepo.alpha = 1.0;
        [self.view bringSubviewToFront:self.viewNoRepo];
    }else {
        self.viewNoRepo.alpha = 0.0;
        self.labelTitle.text = [SUDataManager sharedManager].selectedRepository.name;
        self.labelStars.text = [NSString stringWithFormat:@"%lu",(unsigned long)[[SUDataManager sharedManager].selectedRepository.stargazers_count integerValue]];
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd MMMM yyyy  HH:mm:ss"];
        [formatter setLocale:[NSLocale currentLocale]];
    
        self.labelCreated.text = [formatter stringFromDate:[SUDataManager sharedManager].selectedRepository.created_at];
        self.labelUpdated.text = [formatter stringFromDate:[SUDataManager sharedManager].selectedRepository.updated_at];
        self.labelLanguage.text = [SUDataManager sharedManager].selectedRepository.language;
        [self.imageViewAvatar sd_setImageWithURL:[NSURL URLWithString:[SUDataManager sharedManager].selectedRepository.owner.avatar_url]];
        self.labelAutor.text = [SUDataManager sharedManager].selectedRepository.owner.login;
        self.textViewDescription.text = [SUDataManager sharedManager].selectedRepository.repoDescription;
        self.labelWatchers.text = [NSString stringWithFormat:@"%lu",(unsigned long)[[SUDataManager sharedManager].selectedRepository.watchers_count integerValue]];
    }
}

#pragma mark - Actions
/*!
 * @brief show web view with selected repository url
 */

- (IBAction)actionbuttonShowURL:(id)sender {
    SUWebViewVC * vcWebView = [self.storyboard instantiateViewControllerWithIdentifier:@"SUWebViewVC"];
    [self.navigationController pushViewController:vcWebView animated:YES];
}
#pragma mark - UISplitViewControllerDelegate
-(void)splitViewController:(UISplitViewController *)svc
    willHideViewController:(UIViewController *)aViewController
         withBarButtonItem:(UIBarButtonItem *)barButtonItem
      forPopoverController:(UIPopoverController *)pc
{
    
    
    //Set the title of the bar button item
    barButtonItem.title = @"Search";
    
    //Set the bar button item as the Nav Bar's leftBarButtonItem
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
}

-(void)splitViewController:(UISplitViewController *)svc
    willShowViewController:(UIViewController *)aViewController
 invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSLog(@"Will show left side");
}

#pragma mark - RepositorySelectionDelegate
-(void) repositorySelected {
    NSLog(@"dfdf");
    [self fillView];
}
@end

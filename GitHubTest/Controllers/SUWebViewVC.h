//
//  SUWebViewVC.h
//  GitHubTest
//
//  Created by sergey on 15.11.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUBaseVC.h"

/*!
 * @brief Web view controller , show webSite for selected repository
 */
@interface SUWebViewVC : SUBaseVC

@end

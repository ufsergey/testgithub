//
//  AppDelegate.m
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import "AppDelegate.h"
#import "SUMasterVC.h"
#import "SUDetailsVC.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"]];

    [[SUAPIManager sharedManager] setBaseURL:(NSString*)[dictionary objectForKey:@"ServerBaseUrl"]];
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Model"];
    [SUDataManager sharedManager].expirationInterval = 600;
    [SUDataManager sharedManager].repeatCount = 3;
    [SUDataManager clearOldData] ;
    [SDImageCache sharedImageCache].maxCacheAge = 600;
    
    //iPad
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    
        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        
        UINavigationController *leftNavController = [splitViewController.viewControllers objectAtIndex:0];
        UINavigationController *rightNavController = [splitViewController.viewControllers objectAtIndex:1];
        SUMasterVC *masterViewController = (SUMasterVC *)[leftNavController topViewController];
        SUDetailsVC *detailsViewController = (SUDetailsVC *)[rightNavController topViewController];
        splitViewController.delegate = detailsViewController;
        masterViewController.delegate = detailsViewController;
        
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

//
//  AppDelegate.h
//  GitHubTest
//
//  Created by sergey on 21.10.15.
//  Copyright (c) 2015 sergey. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

